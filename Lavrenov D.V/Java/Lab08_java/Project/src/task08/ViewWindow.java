package task08;

import java.awt.Dimension;

public class ViewWindow extends ViewResultOfPerformingClass {

    private Window window = null;

    public ViewWindow() {
        super();
        window = new Window(this);
        window.setSize(new Dimension(640,480));
        window.setTitle("Graphic");
        window.setVisible(true);
        window.setResizable(false);
    }

    @Override
    public void showingOfClass() {
        super.showingOfClass();
        window.setVisible(true);
        window.repaint();
    }
}
