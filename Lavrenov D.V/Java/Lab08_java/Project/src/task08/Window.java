package task08;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class Window extends Frame {

    private static final int BORDER = 40;

    private ViewResultOfPerformingClass view;

    public Window(ViewResultOfPerformingClass view) {
        this.view = view;
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                setVisible(false);
            }
        });
    }

    @Override
    public void paint(Graphics g) {
        Rectangle r = getBounds();
        Rectangle c = new Rectangle();
        r.x += BORDER;
        r.y += 25 + BORDER;
        r.width -= r.x + BORDER;
        r.height -= r.y + BORDER;
        c.x = r.x;
        c.y = r.y + r.height / 2;
        c.width = r.width;
        c.height = r.height / 2;
        g.setColor(Color.LIGHT_GRAY);
        g.setColor(Color.RED);
        g.drawLine(c.x, c.y-4, c.x + c.width, c.y-4);
        g.drawLine(c.x, r.y-10, c.x ,r.y + 183);
        double maxX = 10;
        double maxY = 255;
        double scaleX;
        double scaleY;
        g.drawString("+" + 255, r.x, r.y);
        g.drawString("+" + (255-15), r.x, r.y+15);
        g.drawString("+" + (255-35), r.x, r.y+30);
        g.drawString("+" + (255-65), r.x, r.y+45);
        g.drawString("+" + (255-95), r.x, r.y+60);
        g.drawString("+" + (255-110), r.x, r.y+75);
        g.drawString("+" + (255-130), r.x, r.y+90);
        g.drawString("+" + (255-145), r.x, r.y+105);
        g.drawString("+" + (255-165), r.x, r.y+120);
        g.drawString("+" + (255-185), r.x, r.y+135);
        g.drawString("+" + (255-205), r.x, r.y+150);
        g.drawString("+" + (255-235), r.x, r.y+165);
        g.drawString("+" + (255-250), r.x, r.y+180);
        g.drawString(""+10, c.x+10 + c.width - g.getFontMetrics().stringWidth(""+maxX), c.y+8);
        g.drawString(""+9, c.x-40 + c.width - g.getFontMetrics().stringWidth(""+maxX), c.y+8);
        g.drawString(""+8, c.x-96 + c.width - g.getFontMetrics().stringWidth(""+maxX), c.y+8);
        g.drawString(""+7, c.x-152 + c.width - g.getFontMetrics().stringWidth(""+maxX), c.y+8);
        g.drawString(""+6, c.x-207 + c.width - g.getFontMetrics().stringWidth(""+maxX), c.y+8);
        g.drawString(""+5, c.x-264 + c.width - g.getFontMetrics().stringWidth(""+maxX), c.y+8);
        g.drawString(""+4, c.x-320 + c.width - g.getFontMetrics().stringWidth(""+maxX), c.y+8);
        g.drawString(""+3, c.x-376 + c.width - g.getFontMetrics().stringWidth(""+maxX), c.y+8);
        g.drawString(""+2, c.x-432 + c.width - g.getFontMetrics().stringWidth(""+maxX), c.y+8);
        g.drawString(""+1, c.x-487 + c.width - g.getFontMetrics().stringWidth(""+maxX), c.y+8);


        scaleX = c.width / maxX;
        scaleY = c.height / maxY;
        g.setColor(Color.black);

        for(int counterOfShow = 0;
            counterOfShow < view.getArrayList().size();
            counterOfShow++) {
            g.drawOval(
                    c.x + (int)((counterOfShow+1) * scaleX) - 5,
                    c.y - (int)((view.getArrayList().get(counterOfShow).getNumber().getDecimalNumber()) * scaleY) - 5,
                    1,
                    1);
        }

        for(int counterOfShow = 0;
            counterOfShow < view.getArrayList().size()-1;
            counterOfShow++) {
            g.drawLine(c.x + (int)((counterOfShow+1) * scaleX) - 5,
                    c.y - (int)((view.getArrayList().get(counterOfShow).getNumber().getDecimalNumber()) * scaleY) - 5,
                    c.x + (int)((counterOfShow+2) * scaleX) - 5,
                    c.y - (int)((view.getArrayList().get(counterOfShow+1).getNumber().getDecimalNumber()) * scaleY) - 5
            );

        }

    }
}
