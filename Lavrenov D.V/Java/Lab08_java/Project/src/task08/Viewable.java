package task08;

public interface Viewable {

    /**
     * Create the object that realizes {@linkpain View}
     */
    public View getViewOfPerformingClass();

}

