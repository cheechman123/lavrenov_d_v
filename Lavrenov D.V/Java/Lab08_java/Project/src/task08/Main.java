package task08;

public class Main {

    public static void main(String[] args) {
        Application application = new Application(
                new ViewableWindow().getViewOfPerformingClass());
        application.menu();
    }

}
