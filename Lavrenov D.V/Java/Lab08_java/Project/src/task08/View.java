package task08;

public interface View {

    void initialization();
    void showingOfClass();
    void convertationOfClass();
    void showHeaderOfClass();
    void showFooterOfClass();
    void recordingInFile();
    void loadingFromFile();
    void erasingValueOfClass();
    void cleaningOfInstanceOfClass();

}

