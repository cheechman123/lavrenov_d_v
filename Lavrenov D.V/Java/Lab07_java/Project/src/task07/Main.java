package task07;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class Main {
    private final static Main main = new Main();
    public static Main getInstance(){
        return main;
    }

    static List<Window> windowList = new ArrayList<>();

    public static void init() {
        for (int i = 0; i < 5; i++) {

            Window window = new Window((int) (Math.random() * 30), (int) (Math.random() * 30), (int) (Math.random() * 3 + 1));
            Calc calc = new Calc();
            calc.Perimeter(window.getWidth(), window.getLength());
            calc.Square(window.getWidth(), window.getLength());
            calc.Volume(window.getWidth(), window.getLength(), window.getHeight());
            window.setCalc(calc);
            windowList.add(window);
        }
    }
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {

        System.out.println(new SuperWindow().PolimorphMethod());
        System.out.println(new Window().PolimorphMethod());

        Menu menu = new Menu();
        main.init();

        for (Window circleRoom : windowList)
            System.out.println(circleRoom);
        System.out.println("//////////////////////////");

        AddNewRoom addNewRoom = new AddNewRoom();
        addNewRoom.setWindowList(windowList);

        MediumRoom mediumRoom = new MediumRoom();
        mediumRoom.setWindowList(windowList);

        SortRoom sortRoom = new SortRoom();
        sortRoom.setWindowList(windowList);

        SearchRoom searchRoom = new SearchRoom();
        searchRoom.setWindowList(windowList);
        searchRoom.setWindow(windowList.get(windowList.size() - 1));

        MinRoom minRoom = new MinRoom();
        minRoom.setWindowList(windowList);

        menu.addCommand(addNewRoom);
        menu.addCommand(mediumRoom);
        menu.addCommand(searchRoom);
        menu.addCommand(minRoom);
        menu.addCommand(sortRoom);

//        new Thread(() -> {
//            for (int i = 0; i < 5; i++) {
//                menu.execute(menu.getCommandList().get(i));
//            }
//        }).start();
        System.out.println("\n\n\n////////////////////////////////////////////////////////////////////////");

        Publisher publisher = new Publisher();
        publisher.addListener(new Subcriber());
        publisher.addListener(new Subcriber());

        publisher.createNewMessage("Message!!!!!");

        Class classPublisher = Publisher.class;
        Field field = classPublisher.getDeclaredField("str");
        field.setAccessible(true);
        System.out.println(field.get(publisher));
    }
}
