package lab05;


import java.util.ArrayList;
import java.util.List;

public class Main {

    static List<Window> windowList = new ArrayList<>();

    public static void init() {
        for (int i = 0; i < 5; i++) {

            Window window = new Window((int) (Math.random() * 30), (int) (Math.random() * 30), (int) (Math.random() * 3 + 1));
            Calc calc = new Calc();
            calc.Perimeter(window.getWidth(), window.getLength());
            calc.Square(window.getWidth(), window.getLength());
            calc.Volume(window.getWidth(), window.getLength(), window.getHeight());
            window.setCalc(calc);
            windowList.add(window);
        }
    }

    public static void main(String[] args) {

        System.out.println(new SuperWindow().PolimorphMethod());
        System.out.println(new Window().PolimorphMethod());

        Menu menu = new Menu();
        init();


        for (Window window : windowList)
            System.out.println(window);
        System.out.println("//////////////////////////");

        AddNewWindow addNewWindow = new AddNewWindow();
        addNewWindow.setWindowList(windowList);

        MediumWindow mediumWindow = new MediumWindow();
        mediumWindow.setWindowList(windowList);

        SortWindow sortWindow = new SortWindow();
        sortWindow.setWindowList(windowList);

        menu.addCommand(addNewWindow);
        menu.addCommand(mediumWindow);
        menu.addCommand(sortWindow);
        menu.execute();
    }
}
