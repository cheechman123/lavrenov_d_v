/*
 * StudyRoom.cpp
 *
 *  Created on: 19 ����. 2018 �.
 *      Author: Daria
 */

#include "StudyRoom.h"
#include "roomType.h"

StudyRoom::StudyRoom() :
		name("Empty"), seats(0), type(UNDEFINED) {

}

StudyRoom::StudyRoom(const StudyRoom &room) :Room(room), name(room.name), seats(room.seats), type(room.type) {

}

unsigned int StudyRoom::getSeats() const {
	return seats;
}
char* StudyRoom::getName() const {
	return name;
}
roomType StudyRoom::getType() const {
	return type;
}

void StudyRoom::setSeats(const unsigned int seats) {
	this->seats = seats;
}
void StudyRoom::setName(char* name) {
	this->name = name;
}
void StudyRoom::setType(const roomType type) {
	this->type = type;
}

StudyRoom& StudyRoom::operator=(const StudyRoom room){
cout<<"Overrided operator =" << endl;
this->height=room.getHeight();
this->setLength(room.getLength());
this->setWidth(room.getWidth());
this->name=room.getName();
this->seats=room.getSeats();
this->type=room.getType();
return *this;

}


StudyRoom::~StudyRoom() {
	//delete name;

}


