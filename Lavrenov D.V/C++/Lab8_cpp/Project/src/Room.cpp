/*
 * Room.cpp
 *
 *  Created on: 14 ����. 2018 �.
 *      Author: Daria
 */

#include "Room.h"

Room::Room() :
		length(0), height(0), width(0) {

}

Room::Room(const Room& room) :
		length(room.length), height(room.height), width(room.width) {

}

Room::~Room() {


}

unsigned int Room::getLength() const {
	return length;
}

unsigned int Room::getHeight() const {
	return height;
}

unsigned int Room::getWidth() const {
	return width;
}

void Room::setLength(const unsigned int length) {
	this->length = length;
}

void Room::setHeight(const unsigned int height) {
	this->height = height;
}

void Room::setWidth(const unsigned int width) {
	this->width = width;
}

unsigned int Room::volume() const {
	return height * width * length;

}

bool Room::operator<(const Room &room){
	return (this->volume()<room.volume());
}

