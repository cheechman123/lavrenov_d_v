#include "header.h"

CWheel::CWheel(){
	/*cout<<"constructor  is worked"<<endl;*/
}

CWheel::~CWheel(void){
	/*cout<<"destructor is worked"<<endl;*/
}

void CWheel::SetDiam(int aDiametr) {
	iDiametr = aDiametr;
}

void CWheel::SetWidth(int aWidth){
	 iWidth = aWidth;
}

void CWheel::SetUnit(string aUnit){
	sUnit = aUnit;
}

int CWheel::GetDiam() const{
	return iDiametr;
}

int CWheel::GetWidth() const{
	return iWidth;
}

string CWheel::GetUnit() const{
	return sUnit;
}

void CView3::SetDataSource(CCar* iCar) {
	iCar->SetDiam(40);
	iCar->SetWidth(80);
	iCar->SetUnit("����");
	iCar->SetRubber("������");
	iCar->SetSize("�������");
}

void CView3::PrintData(){
	cout<<"������� "<<iCar.GetDiam()<<endl;
	cout<<"������ "<<iCar.GetWidth()<<endl;
	cout<<"������� ��������� "<<iCar.GetUnit()<<endl;
	cout<<"������ "<<iCar.GetRubber()<<endl;
	cout<<"������ "<<iCar.GetSize()<<endl;
}

const int CWheel::Volume(const CWheel& iWheel) const{
	double pi = 3.14;
	int  radius = iWheel.GetDiam()/2;
	int shirina = iWheel.GetWidth();
	double volume = pi*radius*radius*shirina;
	cout<<"\\\\\\\\\\\\\\\����� ������\\\\\\\\\\\\\\\\\\ = "<<volume<<" "<<iWheel.GetUnit()<<endl;
	return 0;
}

void CCar::SetRubber(string aRubber){
	sRubber = aRubber;
}

void CCar::SetSize(string aSize){
	sSize = aSize;
}

string CCar::GetRubber() const{
	return sRubber;
}

string CCar::GetSize() const{
	return sSize;
}

CCar::CCar(){
	/*cout<<"constructor2  is worked"<<endl;*/
}

CCar::~CCar(){
	/*cout<<"destructor2 is worked"<<endl;*/
}

void CView1::method(const CCar& iCar){
	cout << "-----------�����������-----------" << endl;
	cout<<"������� "<<iCar.GetDiam()<<endl;
	cout<<"������ "<<iCar.GetWidth()<<endl;
	cout<<"������� ��������� "<<iCar.GetUnit()<<endl;
	cout << "---------------------------------" << endl;
}

int CWheel::operator==(const CWheel& com)
{
	if (this->iDiametr == com.iDiametr || this->sUnit == com.sUnit)
	{
		return true;
	}
	return false;
}

bool CCar::operator=(string a)
{
	this->sRubber = a;
	return true;
}

void CView2::OnTimerAction()
{
	Sleep(3000);
}

int CWheel::Volume(const CWheel& iWheel , double pi){
	int  radius = iWheel.GetDiam()/2;
	int width = iWheel.GetWidth();
	int volume = pi*radius*radius*width;
	return volume;
}