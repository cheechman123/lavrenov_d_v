

#ifndef COMPAREMOREOREQUAL_HPP_
#define COMPAREMOREOREQUAL_HPP_

#include "RoomCollection.h"
/**
 * functor for comparing
 */
class CompareMoreOrEqual {
public:
	/**
	 *
	 * @param actual first operand
	 * @param verificator second operand
	 * @return
	 */
	bool operator()(unsigned int actual, unsigned int verificator);
};
/**
 * function for finding rooms with needed condition
 * @param collection collection for searching in
 * @param verificator operand for comparing
 * @param f functor which provides condition of searching
 * @return number of rooms
 */
template<class Functor>
int findRooms(RoomCollection& collection, const int verificator, Functor f) {
	int counter = 0;
	for (int i = 0; i < collection.getSize(); i++) {
		if (f(collection[i]->getSeats(), verificator)) {
			counter++;
		}

	}
	return counter;
}

#endif /* COMPAREMOREOREQUAL_HPP_ */
