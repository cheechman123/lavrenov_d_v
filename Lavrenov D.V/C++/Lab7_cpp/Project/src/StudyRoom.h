

#ifndef SRC_STUDYROOM_H_
#define SRC_STUDYROOM_H_
#include <string>
#include"Room.h"
#include "roomType.h"
/**
 * child class from Room
 * @param seats number of seats in room
 * @param name room name
 * @param type room type
 */
class StudyRoom: public Room {
public:

	/**
	 * default constructor
	 */
	StudyRoom();
	/**
	 * Copy constructor
	 * @param room room for copying
	 */
	StudyRoom(const StudyRoom &room);
	/**
	 * getter for seats
	 * @return number of seats
	 */
	unsigned int getSeats() const;
	/**
	 * getter for name
	 * @return name
	 */
	char* getName() const;
	/**
	 * getter for type
	 * @return type of room
	 */
	roomType getType() const;
	/**
	 * setter for seats
	 * @return number of seats
	 */
	/**
	 * setter for number of seats
	 * @param seats new number of seats
	 */
	void setSeats(const unsigned int seats);
	/**
	 * setter for name
	 * @param name new name
	 */
	void setName(char* name);
	/**
	 * setter for type
	 * @param type new type
	 */
	void setType(const roomType type);
	/**
	 * destructor
	 */
	StudyRoom& operator=(const StudyRoom room);
	~StudyRoom() override;
private:
	unsigned int seats;
	char* name;
	roomType type;

};

#endif /* SRC_STUDYROOM_H_ */
