package task05;

import java.io.*;
interface MyInterface{
    void output();
}
abstract class AbstractClass implements Serializable, MyInterface{
    public void outputName() {
        System.out.println(this.getClass());
    }
    int value = 0;
    int secondValue = 0;
}
class Child extends AbstractClass{

    @Override
    public void output() {
        System.out.println("Interface implemented by:  " + this.getClass());
    }
    @Override
    public void outputName() {
        System.out.println(this.getClass());
    }

}
public class Main extends Child{
    @Override
    public void output() {
        System.out.println("Interface implemented by:  " + this.getClass());
    }
    @Override
    public void outputName() {
        System.out.println(this.getClass());
    }
    public static void main(String[] args) throws IOException {

        AbstractClass abs = new Main();
        abs.output();
        abs.outputName();

        FileOutputStream fos = new FileOutputStream("temp.out");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(abs);
        oos.flush();
        oos.close();
    }
}
