#include <iostream>
#include <stdio.h>

using namespace std;
using std::string;

class Window {

private:
	// ID ����
	int ID;
	// ��������� ����� �� ��� �
	int x1;
	// ��������� ����� �� ��� �
	int y1;
	// �������� ����� �� ��� �
	int y2;
	// �������� ����� �� ��� �
	int x2;

public:
	/**
	* ����������� � ����������� 
	* @param ID1 ID ����
	* @param ax1 ��������� ����� �� ��� �
	* @param ay1 ��������� ����� �� ��� �
	* @param ax2 �������� ����� �� ��� �
	* @param ay2 �������� ����� �� ��� �
	*/
	Window(int ID1, int ax1, int ay1, int ax2, int ay2){
		ID = ID1;
		x1 = ax1;
		y1 = ay1;
		y2 = ay2;
		x2 = ax2;
		cout<<"Window()"<<endl;
	};

	/**
	* ����������� ��� ����������
	*/
	Window(){
		cout<<"Window()"<<endl;
	}

	/** 
	* ����������
	*/
	~Window(){
		cout<<"~Window()"<<endl;
	}

	/**
	* ����� ������ ������ �� �����
	*/
	void output(){
		cout<<"Window ID: "<<ID<<"\nCoordinates:\nX1: "<<x1<<"\nY1: "<<y1<<"\nX2: "<<x2<<"\nY2: "<<y2<<endl;
	}
};

void main() {
	Window* wind = new Window(12345, 0, 0, 200, 150);
	wind->output();
	cout<<endl;
	delete wind;
}